// 1. Реалізувати функцію підсвічування клавіш.
//
//- У файлі index.html лежить розмітка для кнопок.
//
// - Кожна кнопка містить назву клавіші на клавіатурі
//
// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір.
// При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною.
// Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір.
// Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
//
// Але якщо при натискані на кнопку її не існує в розмітці, то попередня активна кнопка повина стати неактивною.

const buttons = document.querySelectorAll('.btn');


window.addEventListener("keydown", (event) => {

for (let i = 0; i < buttons.length; i++) {

    if (buttons[i].innerText.toLowerCase() === event.key.toLowerCase()) {
        buttons[i].style.background = 'blue';
    } else if (buttons[i].innerText.toLowerCase() !== event.key.toLowerCase()) {
        buttons[i].style.background = 'black';
    }
}
});
